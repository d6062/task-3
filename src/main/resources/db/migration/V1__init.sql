create table if not exists "user"
(
    uid      bigint primary key,
    username text
);

create table if not exists node
(
    id        bigint primary key,
    changeset bigint,
    cdate     timestamp,
    version   integer,
    uid       bigint not null,
    lat       double precision,
    lon       double precision,
    tags_json jsonb,

    constraint osm_node_user_fk
        foreign key (uid) references "user"
);

create table if not exists tag
(
    id      bigserial primary key,
    k       text,
    v       text,
    node_id bigint not null,

    constraint osm_tag_node_fk
        foreign key (node_id) references node
);

create table if not exists tags_embedded
(
    node_id bigint not null,
    key     text,
    value   text,

    constraint osm_tags_embedded_node_fk
        foreign key (node_id) references node
);

