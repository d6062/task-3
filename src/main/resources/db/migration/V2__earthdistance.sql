create extension if not exists earthdistance cascade;

create index if not exists idx_distance_1
    on node (
                 earth_distance(ll_to_earth(lat, lon), ll_to_earth(0, 0))
        );

create index if not exists idx_distance_2
    on node (
                 earth_distance(ll_to_earth(lat, lon), ll_to_earth(0, 90))
        );
