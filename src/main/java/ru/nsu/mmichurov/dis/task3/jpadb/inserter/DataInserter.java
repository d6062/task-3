package ru.nsu.mmichurov.dis.task3.jpadb.inserter;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.mmichurov.dis.task3.NodeUnmarshaller;
import ru.nsu.mmichurov.dis.task3.jpadb.model.Node;
import ru.nsu.mmichurov.dis.task3.jpadb.model.Tag;
import ru.nsu.mmichurov.dis.task3.jpadb.model.User;
import ru.nsu.mmichurov.dis.task3.jpadb.properties.InputProperties;
import ru.nsu.mmichurov.dis.task3.jpadb.repository.NodeRepository;

import javax.persistence.CollectionTable;
import javax.persistence.EntityManager;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Objects;

@Component
@RequiredArgsConstructor
@Log4j2
public class DataInserter {
    private static final int BUFFER_SIZE = 1024 * 1024 * 10;

    private final InputProperties properties;

    private final EntityManager entityManager;
    private final NodeRepository nodeRepository;

    @SneakyThrows
    @EventListener(ApplicationReadyEvent.class)
    @Transactional
    public void insertData() {
        log.info("Using input properties: {}", properties);

        if (properties.clear) {
            log.info("Clearing tables");
            clear();
        }

        if (!properties.insertData) {
            log.info("Will not insert data");
            return;
        }

        log.info("Inserting data");

        try (
                val rawInput = new FileInputStream(properties.fileName);
                val xmlInput = new BZip2CompressorInputStream(new BufferedInputStream(rawInput, BUFFER_SIZE));
                val unmarshaller = new NodeUnmarshaller(xmlInput)
        ) {
            insertData(unmarshaller);
        } catch (Exception e) {
            log.error(e);
            throw e;
        }
    }

    private void insertData(final @NotNull NodeUnmarshaller unmarshaller) {
        val startTimeS = System.nanoTime() / 1e9;
        var elapsedTimeS = 0d;
        var totalInserted = 0L;

        val progressLogger = new ProgressLogger();

        for (val nodeXml : unmarshaller) {
            elapsedTimeS = System.nanoTime() / 1e9 - startTimeS;

            if (elapsedTimeS >= properties.timeLimit || totalInserted >= properties.countLimit) {
                break;
            }

            nodeRepository.save(Node.from(nodeXml));

            totalInserted += 1;
            progressLogger.update(totalInserted);
        }

        progressLogger.done(totalInserted, Math.round(elapsedTimeS));
    }

    private void clear() {
        deleteAllEntities(Tag.class);
        deleteAllEmbedded(Node.class);
        deleteAllEntities(Node.class);
        deleteAllEntities(User.class);
    }

    private <T> void deleteAllEntities(Class<T> entityType) {
        val builder = entityManager.getCriteriaBuilder();
        val query = builder.createCriteriaDelete(entityType);
        query.from(entityType);
        entityManager.createQuery(query).executeUpdate();
    }

    @SneakyThrows
    private <T> void deleteAllEmbedded(@SuppressWarnings("SameParameterValue") Class<T> entityType) {
        Arrays.stream(entityType.getDeclaredFields())
                .map(it -> it.getAnnotation(CollectionTable.class))
                .filter(Objects::nonNull)
                .forEach(it -> {
                    val table = it.name();

                    val deleteQuery = entityManager.createNativeQuery(
                            String.format("truncate table %s", table)
                    );
                    deleteQuery.executeUpdate();
                });
    }

    @RequiredArgsConstructor
    private static class ProgressLogger {
        private final static long FIRST_LOG_COUNT = 1000;

        private long nextLogCount = FIRST_LOG_COUNT;

        public void update(final long inserted) {
            if (inserted < nextLogCount) {
                return;
            }

            DataInserter.log.info("Inserted {} nodes", inserted);
            nextLogCount *= 2;
        }

        public void done(
                final long inserted,
                final long elapsed
        ) {
            DataInserter.log.info("Inserted {} nodes in {} seconds", inserted, elapsed);
        }
    }
}
