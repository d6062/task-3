package ru.nsu.mmichurov.dis.task3.jpadb.dto;

import lombok.Data;

@Data
public class Tag {
    String key;

    String value;
}
