package ru.nsu.mmichurov.dis.task3.jpadb.dto;

import lombok.Data;

import java.util.Collection;
import java.util.Date;

@Data
public class Node {
    long id;

    long version;

    Date timestamp;

    User user;

    long changeSet;

    double latitude;

    double longitude;

    Collection<Tag> tags;
}
