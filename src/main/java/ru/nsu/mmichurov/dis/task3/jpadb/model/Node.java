package ru.nsu.mmichurov.dis.task3.jpadb.model;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "node")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Node {
    @Transient
    public static @NotNull Node empty = new Node();

    @Id
    @Column(name = "id")
    long id;

    @Column(name = "version")
    int version;

    @Column(name = "cdate")
    @NotNull Date date;

    @ToString.Exclude
    @ManyToOne(
            optional = false,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}
    )
    @JoinColumn(name = "uid")
    @NotNull User user;

    @Column(name = "changeset")
    long changeSet;

    @Column(name = "lat")
    double latitude;

    @Column(name = "lon")
    double longitude;

    @ToString.Exclude
    @OneToMany(
            mappedBy = "node",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}
    )
    Collection<Tag> tags;

    public void setTags(final @NotNull Collection<Tag> tags) {
        tags.forEach(it -> it.setNode(this));
        this.tags = tags;
    }

    @ToString.Exclude
    @ElementCollection
    @CollectionTable(
            name = "tags_embedded",
            joinColumns = @JoinColumn(name = "node_id", columnDefinition = "bigint")
    )
    Collection<TagEmbeddable> tagsEmbeddable;

    @ToString.Exclude
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    Collection<TagJson> tagsJson;


    public static @NotNull Node from(final @NotNull org.openstreetmap.osm.Node node) {
        val result = Node.builder()
                .id(node.getId().longValue())
                .version(node.getVersion().intValue())
                .date(node.getTimestamp().toGregorianCalendar().getTime())
                .user(User.from(node))
                .changeSet(node.getChangeset().longValue())
                .latitude(node.getLat())
                .longitude(node.getLon())
                .tagsEmbeddable(TagEmbeddable.allFrom(node))
                .tagsJson(TagJson.allFrom(node))
                .build();

        result.setTags(Tag.allFrom(node));

        return result;
    }
}
