package ru.nsu.mmichurov.dis.task3.jpadb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import ru.nsu.mmichurov.dis.task3.jpadb.properties.InputProperties;

@SpringBootApplication
@EnableConfigurationProperties(InputProperties.class)
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
