package ru.nsu.mmichurov.dis.task3.jpadb.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Collection;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TagJson implements Serializable {
    @NotNull String key;
    @NotNull String value;

    public static @NotNull TagJson from(final @NotNull org.openstreetmap.osm.Tag tag) {
        return TagJson.builder()
                .key(tag.getK())
                .value(tag.getV())
                .build();
    }

    public static @NotNull Collection<TagJson> allFrom(final @NotNull org.openstreetmap.osm.Node node) {
        return node.getTag().stream()
                .map(TagJson::from)
                .collect(Collectors.toSet());
    }
}
