package ru.nsu.mmichurov.dis.task3.jpadb.model;

import lombok.*;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "\"user\"")
public class User {
    @Transient
    public static @NotNull User empty = new User();

    @Id
    @Column(name = "uid")
    long id;

    @Column(name = "username")
    @NotNull String name;

    public static @NotNull User from(final @NotNull org.openstreetmap.osm.Node node) {
        return User.builder()
                .id(node.getUid().longValue())
                .name(node.getUser())
                .build();
    }
}
