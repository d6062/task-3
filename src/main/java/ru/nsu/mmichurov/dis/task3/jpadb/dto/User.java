package ru.nsu.mmichurov.dis.task3.jpadb.dto;

import lombok.Data;

@Data
public class User {
    long id;

    String name;
}
