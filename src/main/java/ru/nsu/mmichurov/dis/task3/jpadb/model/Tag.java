package ru.nsu.mmichurov.dis.task3.jpadb.model;

import lombok.*;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Collection;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "tag")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @ToString.Exclude
    @ManyToOne(optional = false)
    @JoinColumn(name = "node_id")
    @NotNull Node node;

    @Column(name = "k")
    @NotNull String key;

    @Column(name = "v")
    @NotNull String value;

    public static @NotNull Tag from(final @NotNull org.openstreetmap.osm.Tag tag) {
        return Tag.builder()
                .node(Node.empty)
                .key(tag.getK())
                .value(tag.getV())
                .build();
    }

    public static @NotNull Collection<Tag> allFrom(final @NotNull org.openstreetmap.osm.Node node) {
        return node.getTag().stream()
                .map(Tag::from)
                .collect(Collectors.toSet());
    }
}
