package ru.nsu.mmichurov.dis.task3.jpadb.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.mmichurov.dis.task3.jpadb.dto.Node;
import ru.nsu.mmichurov.dis.task3.jpadb.mapper.NodeMapper;
import ru.nsu.mmichurov.dis.task3.jpadb.repository.NodeRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class NodeService {
    private final NodeRepository repository;
    private final NodeMapper mapper;

    public Optional<Node> getById(final long id) {
        return repository.findById(id).map(mapper::toDto);
    }

    public Collection<Node> getWithinRadius(
            final double latitude,
            final double longitude,
            final double radius
    ) {
        return repository.findWithinRadius(latitude, longitude, radius).stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
