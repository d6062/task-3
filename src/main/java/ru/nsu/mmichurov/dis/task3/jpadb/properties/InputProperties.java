package ru.nsu.mmichurov.dis.task3.jpadb.properties;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@RequiredArgsConstructor
@ToString
@ConstructorBinding
@ConfigurationProperties(prefix = "input")
public class InputProperties {
    public final @NotNull String fileName;
    public final boolean insertData;
    public final long timeLimit;
    public final long countLimit;
    public final boolean clear;
}
