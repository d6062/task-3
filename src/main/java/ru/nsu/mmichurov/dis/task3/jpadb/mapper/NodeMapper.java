package ru.nsu.mmichurov.dis.task3.jpadb.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.nsu.mmichurov.dis.task3.jpadb.dto.Node;

@Mapper(componentModel = "spring")
public interface NodeMapper {
    @Mapping(source = "tagsJson", target = "tags")
    @Mapping(source = "date", target = "timestamp")
    Node toDto(ru.nsu.mmichurov.dis.task3.jpadb.model.Node node);
}
