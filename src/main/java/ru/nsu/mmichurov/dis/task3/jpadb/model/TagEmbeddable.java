package ru.nsu.mmichurov.dis.task3.jpadb.model;

import lombok.*;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Embeddable;
import java.util.Collection;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Embeddable
public class TagEmbeddable {
    @NotNull String key;

    @NotNull String value;

    public static @NotNull TagEmbeddable from(final @NotNull org.openstreetmap.osm.Tag tag) {
        return TagEmbeddable.builder()
                .key(tag.getK())
                .value(tag.getV())
                .build();
    }

    public static @NotNull Collection<TagEmbeddable> allFrom(final @NotNull org.openstreetmap.osm.Node node) {
        return node.getTag().stream()
                .map(TagEmbeddable::from)
                .collect(Collectors.toSet());
    }
}
