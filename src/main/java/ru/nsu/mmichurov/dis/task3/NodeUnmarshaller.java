package ru.nsu.mmichurov.dis.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.openstreetmap.osm.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Set;

public final class NodeUnmarshaller implements Closeable, Iterable<Node>, Iterator<Node> {
    private static final Logger log = LogManager.getLogger(NodeUnmarshaller.class);

    private static final Set<String> stopTags = Set.of("way", "relation");

    private final XMLStreamReader reader;
    private final Unmarshaller unmarshaller;

    public NodeUnmarshaller(final @NotNull InputStream in) throws XMLStreamException, JAXBException {
        final var inputFactory = XMLInputFactory.newInstance();
        reader = fixNamespace(inputFactory.createXMLStreamReader(in));

        final var context = JAXBContext.newInstance(Node.class);
        unmarshaller = context.createUnmarshaller();
    }

    private boolean skipElements() throws XMLStreamException {
        while (reader.hasNext()) {
            final var eventType = reader.getEventType();
            if (eventType == XMLStreamConstants.END_DOCUMENT) {
                return false;
            }

            if (eventType != XMLStreamConstants.START_ELEMENT) {
                reader.next();
                continue;
            }

            final var localName = reader.getLocalName();
            if (stopTags.contains(localName)) {
                return false;
            }

            if ("node".equals(localName)) {
                break;
            }

            reader.next();
        }

        return true;
    }

    private static XMLStreamReader fixNamespace(final @NotNull XMLStreamReader reader) {
        return new StreamReaderDelegate(reader) {
            @Override
            public int getNamespaceCount() {
                return 1;
            }

            @Override
            public String getNamespaceURI(String prefix) {
                return getNamespaceURI();
            }

            @Override
            public String getNamespaceURI(int index) {
                return getNamespaceURI();
            }

            @Override
            public String getNamespaceURI() {
                return "http://openstreetmap.org/osm/0.6";
            }

            @Override
            public String getNamespacePrefix(int index) {
                return "osm";
            }
        };
    }

    @Override
    public void close() {
        try {
            reader.close();
        } catch (XMLStreamException e) {
            log.error(e);
        }
    }

    public boolean hasNext() {
        try {
            return skipElements();
        } catch (XMLStreamException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public @NotNull Node next() {
        try {
            skipElements();
            return (Node) unmarshaller.unmarshal(reader);
        } catch (XMLStreamException | JAXBException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public @NotNull Iterator<Node> iterator() {
        return this;
    }
}
