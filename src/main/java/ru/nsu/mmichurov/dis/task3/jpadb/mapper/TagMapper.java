package ru.nsu.mmichurov.dis.task3.jpadb.mapper;

import org.mapstruct.Mapper;
import ru.nsu.mmichurov.dis.task3.jpadb.dto.Tag;

@Mapper(componentModel = "spring")
public interface TagMapper {
    Tag toDto(ru.nsu.mmichurov.dis.task3.jpadb.model.Tag tag);
}
