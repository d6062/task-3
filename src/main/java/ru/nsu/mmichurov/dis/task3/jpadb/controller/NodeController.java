package ru.nsu.mmichurov.dis.task3.jpadb.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.mmichurov.dis.task3.jpadb.dto.Node;
import ru.nsu.mmichurov.dis.task3.jpadb.service.NodeService;

import java.util.Collection;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
public class NodeController {
    private final NodeService service;

    @GetMapping("/node")
    public Optional<Node> getById(@RequestParam final long id) {
        return service.getById(id);
    }

    @GetMapping("/within_radius")
    public Collection<Node> getById(
            @RequestParam double lat,
            @RequestParam double lon,
            @RequestParam double radius
    ) {
        return service.getWithinRadius(lat, lon, radius);
    }
}
