package ru.nsu.mmichurov.dis.task3.jpadb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.nsu.mmichurov.dis.task3.jpadb.model.Node;

import java.util.Collection;
import java.util.Optional;

public interface NodeRepository extends JpaRepository<Node, Long> {
    @Query(
            value =
                    """
                    with distances(
                        id,
                        other_distance_1,
                        other_distance_2,
                        origin_distance_1,
                        origin_distance_2
                    ) as (
                        select
                            id,
                            public.earth_distance(public.ll_to_earth(node.lat, node.lon), public.ll_to_earth(0, 0)),
                            public.earth_distance(public.ll_to_earth(node.lat, node.lon), public.ll_to_earth(0, 90)),
                            public.earth_distance(public.ll_to_earth(0, 0), public.ll_to_earth(:lat, :lon)),
                            public.earth_distance(public.ll_to_earth(0, 90), public.ll_to_earth(:lat, :lon))
                        from
                            public.node node
                    ),
                    candidates(
                        id,
                        distance
                    ) as (
                        select
                            other.id,
                            public.earth_distance(public.ll_to_earth(other.lat, other.lon), public.ll_to_earth(:lat, :lon))
                        from
                            public.node other
                            join distances on other.id = distances.id
                        where
                            distances.other_distance_1
                                between distances.origin_distance_1 - :radius
                                and distances.origin_distance_1 + :radius
                            and distances.other_distance_2
                                between distances.origin_distance_2 - :radius
                                and distances.origin_distance_2 + :radius
                    )
                                
                    select
                        candidates.distance,
                        other.*
                    from
                        public.node other
                        join candidates on other.id = candidates.id
                    where
                        candidates.distance < :radius
                    order by
                        candidates.distance
                    """,
            nativeQuery = true
    )
    Collection<Node> findWithinRadius(
            double lat,
            double lon,
            double radius
    );

    Optional<Node> findById(long id);
}
