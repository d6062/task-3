package ru.nsu.mmichurov.dis.task3.jpadb.mapper;

import org.mapstruct.Mapper;
import ru.nsu.mmichurov.dis.task3.jpadb.dto.User;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User toDto(ru.nsu.mmichurov.dis.task3.jpadb.model.User user);
}
