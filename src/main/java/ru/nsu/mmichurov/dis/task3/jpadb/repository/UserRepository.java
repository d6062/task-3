package ru.nsu.mmichurov.dis.task3.jpadb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.mmichurov.dis.task3.jpadb.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
}
